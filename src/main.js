import Vue from 'vue'
import App from './App.vue'
import '../public/css/color-palette.css'
import '../public/css/layout.css'
import '../public/css/style.css'
import '../public/css/button.css'
import '../public/css/card.css'
import '../public/css/input.css'
import '../public/css/icons.css'
import '../public/css/form.css'
import '../public/css/cast-card.css'
import '../public/css/week-forecast.css'
import '../public/css/footer.css'
import '../public/css/week-forecast.css'

import axios from 'axios'
Vue.config.productionTip = false
window.axios = axios;
Vue.environement= Vue.prototype.environement = [window.location.protocol, window.location.hostname].join("//");
new Vue({
  render: h => h(App),
}).$mount('#app')
