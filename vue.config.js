module.exports = {
    filenameHashing: false,
    devServer: {
      proxy: {
        '^/api': {
          target: 'https://weathersound.fr',
          changeOrigin: true
        },
      }
    }
  }
